#!/usr/bin/env bash

./tf_docs.sh

cd ../tf.site

terraform init
terraform fmt
terraform validate
terraform plan
#terraform apply