# mergedamage, merge da mage, merged a mage?, merge damage, Merge Damage!

Terraform that creates all the AWS infrastructure needed to run my static web site - https://mergedamage.com

## Features
* HTML files hosted in an S3 bucket
* TLS Certificate
* Redirect of port 80 to 443
* Redirect of "https://www.mergedamage.com" to "https://mergedamage.com"
* Cloudfront hosting the TLS
* Convenience TF modules

## Why?
I wanted to learn how to do it in Terraform - so I read a few things online, copied examples, made some tweaks & did it.

Inspired by https://medium.com/runatlantis/hosting-our-static-site-over-ssl-with-s3-acm-cloudfront-and-terraform-513b799aec0f<br/>

### Biggest tweaks
* DNS for authenticating the certificate, instead of emails
* I prefer root domains instead of www, so www redirects to root
* directory structure and organization of code into terraform modules
* scripts to do the common things so that I don't have to remember if I don't touch Terraform in a few years
* README generation & documentation

### Left to do
* [ ] Find a way to combine the tf modules s3_web_redirect and s3_web_site.  Both files are only 100 lines, but there are only 3 code lines that differ between them.  Copy'n'paste code makes me feel .. uncomfortable
* [ ] expose more variables in the s3_web_* modules that are more relevant to my html development flow. That will come with time
* [x] Move `terraform.tfstate` to it's own S3 bucket 
* [ ] fix provider for the modules
* [ ] `.gitlab-ci.yml` file that will generate the README.md files, run `plan`, and finally `apply` on master branch
* [x] move the modules to their own repos

## Terraform Modules 

| Resource                       | Desc          |
|--------------------------------|---------------|
| [tf.site/main](tf.site)                                  | assembles the pieces for our site |
| [https://gitlab.com/jasondearte/tf.dv_cert](https://gitlab.com/jasondearte/tf.dv_cert)                 | A "Let's Encrypt"-ish clone for AWS that proves ownership by creating a DNS entry on the domain  |
| [https://gitlab.com/jasondearte/tf.s3_web_redirect.git](https://gitlab.com/jasondearte/tf.s3_web_redirect.git) | s3+cloudfront to set up a web page redirect for a domain |
| [https://gitlab.com/jasondearte/tf.s3_web_site.git](https://gitlab.com/jasondearte/tf.s3_web_site.git)         | s3+cloudfront static web site hosting |

## Lessons learned

Few things that I was not expecting but learned about in the course of this project

### us-east-1
Cloudfront only looks for certs in us-east-1<br/>
https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cnames-and-https-requirements.html


### Cloudfront
it's cloudFRONT not cloudFORMATION.  Ugh.  English for this native speaker is still hard `:(`

Updates to it can also be very slow.  When you're wondering why it's not working, it may be that is the `Status` is still `In Progress`.  Or `State` may not be `Enabled`

https://console.aws.amazon.com/cloudfront/home

https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/troubleshooting.html

### Certs
Most samples I've seen have been for email validation.  This isn't optimal when you are setting up a new domain.

Creating and destroying certs is a slow process.  You may need to run multiple `terraform apply` to get it done if you make changes

### Route53 records for root domains
the `name` should be blank for root domains

### S3
Can't delete a bucket that has files in it.  It's frustrating the first time you experence it, but it's well worth it as this simple safeguard can save your butt at work.
