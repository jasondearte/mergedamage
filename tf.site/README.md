# Merge damage dot com
Create a s3 backed static web site


## Outputs

| Name | Description |
|------|-------------|
| cert_validation_url | certificate validation url |
| redirects | Map of all redirects |
| sites | list of all sites |

# Terraform Version
Terraform v0.11.8

