# ../../terraform_backend project created the state bucket

terraform {
  backend "s3" {
    bucket         = "mergedamagetfs"
    key            = "terraformstate/mergedamage_tf"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
  }
}
