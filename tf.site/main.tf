/**
* # Merge damage dot com
* Create a s3 backed static web site
*/

provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "$HOME/.aws/credentials"
}

provider "aws" {
  alias                   = "global"
  region                  = "us-east-1"
  shared_credentials_file = "$HOME/.aws/credentials"
}

provider "aws" {
  alias                   = "oregon"
  region                  = "us-west-2"
  shared_credentials_file = "$HOME/.aws/credentials"
}

locals {
  domain_name      = "mergedamage.com"
  root_domain_name = "${local.domain_name}"
  www_domain_name  = "www.${local.domain_name}"

  page_index    = "index.html"
  page_error    = "error.html"
  ttl_default   = "3600"           #3600 == 60x60 == 1 hour
  ssl_protocols = ["TLSv1.2"]      #["TLSv1.2_2018"] #["TLSv1", "TLSv1.1", "TLSv2.1"]
  price_class   = "PriceClass_100"

  common_tags = {
    project     = "mergedamage"
    environment = "production"
  }
}

data "aws_route53_zone" "zone" {
  name         = "${local.domain_name}."
  private_zone = false
  provider     = "aws.global"
}

//
// Domain Validation (DV) TLS Cert for the domain
//
module "dv_cert" {
  source      = "git::https://gitlab.com/jasondearte/tf.dv_cert.git?ref=master"
  root_domain = "${local.domain_name}"
  record_ttl  = "3600"
}

//
// Bucket to host the web page
//
module "web_site" {
  source        = "git::https://gitlab.com/jasondearte/tf.s3_web_site.git?ref=master"
  web_domain    = "${local.root_domain_name}"
  root_domain   = "${local.root_domain_name}"
  zone_id       = "${data.aws_route53_zone.zone.zone_id}"
  tags          = "${local.common_tags}"
  cert_arn      = "${module.dv_cert.arn}"
  price_class   = "${local.price_class}"
  ttl_default   = "${local.ttl_default}"
  ssl_protocols = "${local.ssl_protocols}"
  page_index    = "${local.page_index}"
  page_error    = "${local.page_error}"
}

//
// www.domain.tld to domain.tld redirect
//
module "web_redirect" {
  source        = "git::https://gitlab.com/jasondearte/tf.s3_web_redirect.git?ref=master"
  web_domain    = "${local.www_domain_name}"
  root_domain   = "${local.root_domain_name}"
  zone_id       = "${data.aws_route53_zone.zone.zone_id}"
  tags          = "${local.common_tags}"
  cert_arn      = "${module.dv_cert.arn}"
  price_class   = "${local.price_class}"
  ttl_default   = "${local.ttl_default}"
  ssl_protocols = "${local.ssl_protocols}"
  redirect      = "https://${local.root_domain_name}"
}

output "cert_validation_url" {
  description = "certificate validation url"
  value       = "${module.dv_cert.validation_url}"
}

output "redirects" {
  description = "Map of all redirects"

  value = {
    "${module.web_redirect.fqdn}" = "${module.web_redirect.target}"
  }
}

output "sites" {
  description = "list of all sites"

  value = [
    "${module.web_site.fqdn}",
  ]
}
